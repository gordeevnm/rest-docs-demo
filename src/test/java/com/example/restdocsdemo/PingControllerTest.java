package com.example.restdocsdemo;

import com.epages.restdocs.apispec.ConstrainedFields;
import com.example.restdocsdemo.dto.PingDto;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static com.epages.restdocs.apispec.ResourceDocumentation.resource;
import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.responseHeaders;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * gordeevnm@gmail.com
 * 10/7/19
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class PingControllerTest {
    @Rule
    public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation();

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(this.context)
                .apply(documentationConfiguration(this.restDocumentation))
                .build();
    }

    @Test
    public void pingGetExample() throws Exception {
        this.mockMvc.perform(get("/ping").contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andDo(
                        document(
                                "ping-get",
                                resource("GET ping endpoint"),
                                responseFields(
                                        fieldWithPath("data").description("Some response data")
                                ),
                                responseHeaders(headerWithName("Content-Type")
                                        .description("The Content-Type of the payload"))
                        )
                );
    }

    private final ConstrainedFields constrainedFields = new ConstrainedFields(PingDto.class);

    @Test
    public void pingPostExample() throws Exception {
        this.mockMvc
                .perform(
                        post("/ping")
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content("{\"data\": \"ping\"}")
                )
                .andExpect(status().isCreated())
                .andDo(document(
                        "ping-post",
                        resource("POST ping endpoint"),
                        requestFields(
                                constrainedFields.withPath("data").description("String field with validation")
                        ),
                        responseFields(fieldWithPath("data").description("Response data")),
                        responseHeaders(headerWithName("Content-Type").description("The Content-Type of the payload"))
                ));
    }
}
