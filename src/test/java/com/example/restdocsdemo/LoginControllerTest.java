package com.example.restdocsdemo;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static com.epages.restdocs.apispec.ResourceDocumentation.resource;
import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.responseHeaders;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * gordeevnm@gmail.com
 * 10/7/19
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class LoginControllerTest {
    @Rule
    public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation();

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(this.context)
                .apply(documentationConfiguration(this.restDocumentation))
                .build();
    }

    @Test
    public void loginPostExample() throws Exception {
        this.mockMvc
                .perform(
                        post("/login")
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content("{\"username\": \"admin\", " +
                                        "\"password\": \"ya_admin_123\"}")
                )
                .andExpect(status().isOk())
                .andDo(document(
                        "Success login",
                        resource("login endpoint"),
                        requestFields(
                                fieldWithPath("username").description("Username field"),
                                fieldWithPath("password").description("Password field")
                        ),
                        responseFields(
                                fieldWithPath("username").description("Client username"),
                                fieldWithPath("name").description("Client name")
                        ),
                        responseHeaders(headerWithName("Content-Type")
                                .description("The Content-Type of the payload"))
                ));
    }

    @Test
    public void loginPostFailExample() throws Exception {
        this.mockMvc
                .perform(
                        post("/login")
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content("{\"username\": \"admin\", " +
                                        "\"password\": \"ya_admin123\"}")
                )
                .andExpect(status().isUnauthorized())
                .andDo(document(
                        "Failed login",
                        resource("login endpoint"),
                        responseFields(
                                fieldWithPath("error").description("Error description"),
                                fieldWithPath("message").description("Error message")
                        ),
                        responseHeaders(headerWithName("Content-Type")
                                .description("The Content-Type of the payload"))
                ));
    }
}
