package com.example.restdocsdemo;

import com.example.restdocsdemo.dto.CreateUserDto;
import com.example.restdocsdemo.dto.UserDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * gordeevnm@gmail.com
 * 10/7/19
 */
@RestController
public class LoginController {
    @RequestMapping(value = "/login", method = POST)
    public ResponseEntity<Object> asd(@RequestBody CreateUserDto login) {
        if (login.getUsername().equals("admin") && login.getPassword().equals("ya_admin_123")) {
            return new ResponseEntity<>(new UserDto(login.getUsername(), "Admin"), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("{\"error\": \"bad credentials\", \"message\": \"fuck off\"}", HttpStatus.UNAUTHORIZED);
        }
    }
}
