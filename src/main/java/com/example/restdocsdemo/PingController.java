package com.example.restdocsdemo;

import com.example.restdocsdemo.dto.PingDto;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 * gordeevnm@gmail.com
 * 10/7/19
 */
@RestController
public class PingController {
    @RequestMapping(value = "/ping", method = RequestMethod.GET)
    public PingDto ping() {
        return new PingDto("ping");
    }

    @RequestMapping(value = "/ping", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public PingDto ping(@RequestBody PingDto body) {
        return new PingDto(body.data.equals("ping") ? "pong" : "ping");
    }
}
