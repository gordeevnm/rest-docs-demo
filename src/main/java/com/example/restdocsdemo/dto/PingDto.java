package com.example.restdocsdemo.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * gordeevnm@gmail.com
 * 10/7/19
 */
@NoArgsConstructor
@AllArgsConstructor
public class PingDto {
    @NotNull
    @Size(min = 3, max = 10)
    public String data;
}
